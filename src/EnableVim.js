import { Package } from "plugins/package.js";
import { Path } from "runtime/core/lib/path.js";
Package.require(Path.resolve($.binpath, "plugins", "vim", "package.json"));
Package.install("vim");
